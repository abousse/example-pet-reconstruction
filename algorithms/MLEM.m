function x = MLEM(y,attn,A,AT,param)

N = size(y,1) ;
NZ = size(y,2) ;

x = ones(N,N,NZ) ;

norm = param.tau*AT(attn) ;

for k = 1 : param.niter
    
    disp(['iteration ',num2str(k)])
    
    exp_sino = param.tau*attn.*A(x) + param.bg ;
    ratio = y./exp_sino ;
    ratio(isnan(ratio)) = 0 ;
    bproj = param.tau*AT(attn.*ratio) ; 
    
    x = x.*bproj./norm ;
    
    imagesc(x(:,:,10),[0,15]) ; axis image ; colormap hot
    pause(0.1)


end