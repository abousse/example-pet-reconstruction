function sino = forwardProj(im,param)

N = size(im,1) ;
NZ = size(im,3) ;
angles = param.angles ;
Nangles = length(param.angles) ;


sigma_mm = param.fwhm/2.3555 ;
sigma_vox = sigma_mm/param.voxsize ;
Npsf = round(N/5) ;


if (mod(Npsf,2)==0)
    h = fspecial('gaussian',[1 Npsf-1],sigma_vox) ;
else
    h = fspecial('gaussian',[1 Npsf],sigma_vox) ;
end

H = {h,h,h} ;

sino = zeros(N,NZ,Nangles) ;

if (param.gpu == 1)
    im = gpuArray(single(im)) ;
    sino = gpuArray(single(sino)) ;
end

im = convnsep(H,im,'same') ; % convolution with PSF

for i = 1 : Nangles
    
    im_rot = imrotate(im,angles(i),'bilinear','crop') ;
    sino(:,:,i) = squeeze(sum(im_rot,1)) ;
    
end

if param.gpu
    sino = gather(sino) ;
end

