function im = backProj(sino,param)

N = size(sino,1) ;
NZ = size(sino,2) ;
angles = param.angles ;
Nangles = length(param.angles) ;


sigma_mm = param.fwhm/2.3555 ;
sigma_vox = sigma_mm/param.voxsize ;
Npsf = round(N/5) ;


if (mod(Npsf,2)==0)
    h = fspecial('gaussian',[1 Npsf-1],sigma_vox) ;
else
    h = fspecial('gaussian',[1 Npsf],sigma_vox) ;
end

H = {h,h,h} ;
im = zeros(N,N,NZ) ;

if (param.gpu == 1)
    im = gpuArray(single(im)) ;
    sino = gpuArray(single(sino)) ;
end


for i = 1 : Nangles
    r_sino = repmat(reshape(sino(:,:,i),1,N,NZ  )   ,[N 1 1]) ;
    im = im + imrotate(r_sino,-angles(i),'bilinear','crop') ;
end


im = convnsep(H,im,'same') ;

if (param.gpu == 1)
    im = double(gather(im)) ;
end
