clear all
close all

addpath('./algorithms/')
addpath('./ressources/')


xcat = load('./XCAT/XCAT_PETCT_2.mat') ;
activity = xcat.pet ;
mu = xcat.mu_511 ;

N = size(activity,1) ;


imagesc(activity(:,:,10)) ;



param.voxsize = 1 ; % mm
param.fwhm = 5 ; % PET resolution
param.angles = 0:0.5:180 ;
param.tau = 0.05 ; % acquisition time
param.bg = 0.001 ; % scatter + random

param.gpu = 1 ;
param.niter = 300 ;


A = @(x) forwardProj(x, param) ;
AT = @(y) backProj(y, param) ;

attn = exp(-A(mu)) ;

sino = param.tau*attn.*A(activity) + param.bg ;
%sino = poissrnd(sino) ;

recon = MLEM(sino,attn,A,AT,param) ;






